# AngularDuties

## Install dependencies

Run `npm install` inside the folders `server/` and `client/`.

## Execute server

Run `node src/app.js` inside the folder `server/`.

## Execute client

Run `ng serve` inside the folder `client/`. Navigate to `http://localhost:4200/`.
