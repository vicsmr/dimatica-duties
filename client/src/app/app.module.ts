import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoteComponent } from './components/note/note.component';
import { NotesListComponent } from './components/notes-list/notes-list.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from '@angular/common';
import { Store, StoreModule } from '@ngrx/store';
import { reducers } from "./app.state";
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from "./app.effects";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { NotesFormComponent } from './components/notes-form/notes-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';

@NgModule({
  declarations: [
    AppComponent,
    NoteComponent,
    NotesListComponent,
    NotesFormComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    GraphQLModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, {}),
    EffectsModule.forRoot([AppEffects]),
    StoreDevtoolsModule.instrument(),
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule
  ],
  providers: [Store],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
