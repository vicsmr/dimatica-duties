import { createAction  } from '@ngrx/store';
import { Note } from '../models/Note';

export const requestLoadNotes = createAction(
    '[Notes API] request Load Notes',
);
export const getNotes = createAction(
    '[Notes API] Load Notes',
    (notes: Note[]) => ({ notes })
);
export const requestPostNote = createAction(
    '[Notes API] Request Post Note',
    (note: Note) => ({ note })
);
export const postNote = createAction(
    '[Notes API] Post Note',
    (note: Note) => ({ note })
);
export const requestUpdateNote = createAction(
    '[Notes API] Request Update Notes',
    (note: Note) => ({ note })
);
export const updateNote = createAction(
    '[Notes API] Update Notes',
    (note: Note) => ({ note })
);
export const requestDeleteNote = createAction(
    '[Notes API] Request Delete Notes',
    (noteId: string) => ({ noteId })
);
export const deleteNote = createAction(
    '[Notes API] Delete Notes',
    (noteId: string) => ({ noteId })
);