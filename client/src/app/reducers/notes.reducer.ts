import { createReducer, on } from '@ngrx/store';
import { deleteNote, getNotes, postNote, updateNote } from '../actions/notes.actions';
import { Note } from '../models/Note';

export interface NotesShape {
    notes: Note[];
}

export const selectNotes = (state: NotesShape) => state.notes;

export const notesInitialState: NotesShape = {
    notes: []
};

const _notesReducer = createReducer(
    notesInitialState,
    on(getNotes, (state, action): NotesShape => {
        return {
            ...state,
            notes: action.notes,
        };
    }),
    on(postNote, (state, action): NotesShape => {
        return {
            notes: [...state.notes, action.note],
        };
    }),
    on(updateNote, (state, action): NotesShape => {
        return { notes: modifyNotes(state, action)};
    }),
    on(deleteNote, (state, action): NotesShape => {
        return {
            notes: state.notes.filter((note: Note) => note.id !== action.noteId)
        };
    })
);

const modifyNotes = (state: NotesShape, action: any) => {
    return state.notes.map((note: Note) => {
        if (note.id !== action.note.id) {
            return note
        }
    
        return {
            ...note,
            ...action.note
        }
    })
}

export function notesReducer(state: any, action: any) {
    return _notesReducer(state, action);
}