import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Note } from 'src/app/models/Note';
import { AppState, selectNotes } from "../../app.state";
import { requestLoadNotes } from "../../actions/notes.actions";
import { MatDialog } from '@angular/material/dialog';
import { NotesFormComponent } from '../notes-form/notes-form.component';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent implements OnInit {
  notes$: Observable<Note[]>;

  constructor(store: Store<AppState>, private dialog: MatDialog) {
    this.notes$ = store.select(selectNotes);

    store.dispatch(requestLoadNotes());
   }

  ngOnInit(): void {
  }

  openNoteFormDialog() {
    const dialogRef = this.dialog.open(NotesFormComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
