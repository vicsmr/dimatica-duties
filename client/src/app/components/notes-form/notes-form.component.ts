import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { requestPostNote, requestUpdateNote } from 'src/app/actions/notes.actions';
import { AppState } from 'src/app/app.state';
import { Note } from 'src/app/models/Note';

@Component({
  selector: 'app-notes-form',
  templateUrl: './notes-form.component.html',
  styleUrls: ['./notes-form.component.scss']
})
export class NotesFormComponent implements OnInit {
  noteForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private store: Store<AppState>, public dialogRef: MatDialogRef<NotesFormComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Note) {
  }

  ngOnInit(): void {
    const self = this;
    this.noteForm = this.formBuilder.group({
      name: [self.data?.name ? self.data.name : '', [Validators.required, Validators.minLength(4)]]
    });
  }
  
  get getControl(){
    return this.noteForm.controls;
  }
  
  onSubmit(){
    if (this.noteForm.valid) {
      if (this.data) {
        this.store.dispatch(requestUpdateNote({
          ...this.noteForm.value, 
          id: this.data.id
        }));
      } else {
        this.store.dispatch(requestPostNote(this.noteForm.value));
      }
      this.closeDialog();
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }

}
