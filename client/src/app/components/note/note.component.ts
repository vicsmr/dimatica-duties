import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { requestDeleteNote } from 'src/app/actions/notes.actions';
import { AppState } from 'src/app/app.state';
import { Note } from 'src/app/models/Note';
import { NotesFormComponent } from '../notes-form/notes-form.component';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
  @Input() note: Note;

  constructor(private store: Store<AppState>, private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  deleteNote(noteId: string) {
    this.store.dispatch(requestDeleteNote(noteId));
  }

  openModifyNoteForm() {
    const dialogRef = this.dialog.open(NotesFormComponent, {
      width: '250px',
      data: this.note
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
