import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { exhaustMap, map } from "rxjs/operators";
import { GraphQLService, gql } from "./services/graphql.service";
import { requestLoadNotes, getNotes, postNote, updateNote, requestPostNote, requestDeleteNote, deleteNote, requestUpdateNote } from "./actions/notes.actions";

const getNotesQuery = gql`
  query notes {
    notes {
      id
      name
    }
  }
`;

const postNotesQuery = gql`
  mutation($name: String!) {
    addNote(name: $name) {
      id
      name
    }
  }
`;

const updateNotesQuery = gql`
  mutation($id: ID, $name: String!) {
    updateNote(id: $id, name: $name) {
      id
      name
    }
  }
`;

const deleteNotesQuery = gql`
  mutation($id: ID) {
    deleteNote(id: $id) {
      id
    }
  }
`;

@Injectable()
export class AppEffects {
  constructor(readonly graphql: GraphQLService, readonly actions$: Actions) {}

  getNotes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestLoadNotes),
      exhaustMap(() => {
        return this.graphql
          .fetch(getNotesQuery)
          .pipe(
            map((response: any) =>
              getNotes(response.data.notes)
            )
          );
      })
    )
  );

  createNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestPostNote),
      exhaustMap((action) => {
        return this.graphql
          .fetch(postNotesQuery, action.note)
          .pipe(
            map((response: any) =>
              postNote(response.data.addNote)
            )
          );
      })
    )
  );

  updateNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestUpdateNote),
      exhaustMap((action) => {
        return this.graphql
          .fetch(updateNotesQuery, action.note)
          .pipe(
            map((response: any) =>
              updateNote(response.data.updateNote)
            )
          );
      })
    )
  );

  deleteNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestDeleteNote),
      exhaustMap((action) => {
        return this.graphql
          .fetch(deleteNotesQuery, { id: action.noteId })
          .pipe(
            map((response: any) =>
              deleteNote(response.data.deleteNote.id)
            )
          );
      })
    )
  );
}