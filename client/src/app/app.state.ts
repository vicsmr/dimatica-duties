import * as NotesState from "./reducers/notes.reducer";
import { ActionReducerMap, createSelector } from "@ngrx/store";

export interface AppState {
    notes: NotesState.NotesShape;
}
export const reducers: ActionReducerMap<AppState> = {
    notes: NotesState.notesReducer,
};

/**
 * Global Selectors
 */
export const selectNotesState = (state: AppState) => state.notes;
export const selectNotes = createSelector(
  selectNotesState,
  NotesState.selectNotes
);