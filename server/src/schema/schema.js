const graphql = require('graphql');
const Note = require('../models/Note');

const { GraphQLObjectType, GraphQLString, GraphQLList, GraphQLNonNull,
       GraphQLID, GraphQLSchema } = graphql;

const NoteType = new GraphQLObjectType({
    name: 'Note',
    fields: () => ({
        id: { type: GraphQLID  },
        name: { type: GraphQLString }
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        note: {
            type: NoteType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return Note.findById(args.id);
            }
        },
        notes: {
            type: new GraphQLList(NoteType),
            resolve(parent, args) {
                return Note.find({});
            }
        }
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addNote: {
            type: NoteType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                let note = new Note({
                    name: args.name
                });
                return note.save();
            }
        },
        updateNote: {
            type: NoteType,
            args: {
                id: { type: GraphQLID },
                name: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                return new Promise((resolve, reject) => {
                    Note.findOneAndUpdate(
                        {"_id": args.id},
                        { "$set":{name: args.name}},
                        {"new": true}
                    ).exec((err, res) => {
                        if(err) reject(err)
                        else resolve(res)
                    })
                })
            }
        },
        deleteNote: {
            type: NoteType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return Note.findByIdAndDelete(args.id);
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation:Mutation
});