const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const noteSchema = new Schema({
    name: String
});

module.exports = mongoose.model('Note', noteSchema);